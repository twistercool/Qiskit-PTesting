from Qiskit_PTesting.Qiskit_PTesting import TestProperty, Qarg, QiskitPropertyTest
import numpy as np

def create_bell_pair(qc):
    qc.h(0)
    qc.cx(0, 1)
    return qc

def encode_message(qc, msg):
    if msg[1] == "1":
        qc.x(1)
    if msg[0] == "1":
        qc.z(1)
    return qc

def decode_message(qc):
    qc.cx(0, 1)
    qc.h(0)
    return qc

def message(qc, firstQubit=None, secondQubit=None):
    foundZ, foundX = (False, False)
    for gate in qc.data:
        if (gate[0].name == "x"): foundX = True
        if (gate[0].name == "z"): foundZ = True

    intendedMessage = f"{1 if foundZ else 0}{1 if foundX else 0}"

    if firstQubit != None: return firstQubit == intendedMessage[0]
    else: return secondQubit == intendedMessage[1]

class testSuperdense(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=2,
                            nbClassicalBits=2)

    def quantumFunction(self, qc):
        create_bell_pair(qc)
        randMessage = str(f"{np.random.randint(0, 2)}{np.random.randint(0, 2)}")
        encode_message(qc, randMessage)
        decode_message(qc)

    def assertions(self):
        self.assertProbability(0, 1, filter_qc=lambda qc: message(qc, firstQubit="0"))
        self.assertProbability(0, 0, filter_qc=lambda qc: message(qc, firstQubit="1"))
        self.assertProbability(1, 1, filter_qc=lambda qc: message(qc, secondQubit="0"))
        self.assertProbability(1, 0, filter_qc=lambda qc: message(qc, secondQubit="1"))

testSuperdense().run()


class testSuperdenseQuantum(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbTests=4,
                            nbTrials=10,
                            nbMeasurements=30,
                            nbQubits=2,
                            nbClassicalBits=2,
                            backend="ibmq")

    def quantumFunction(self, qc):
        create_bell_pair(qc)
        randMessage = str(f"{np.random.randint(0, 2)}{np.random.randint(0, 2)}")
        encode_message(qc, randMessage)
        decode_message(qc)

    def assertions(self):
        self.assertProbability(0, 1, filter_qc=lambda qc: message(qc, firstQubit="0"))
        self.assertProbability(0, 0, filter_qc=lambda qc: message(qc, firstQubit="1"))
        self.assertProbability(1, 1, filter_qc=lambda qc: message(qc, secondQubit="0"))
        self.assertProbability(1, 0, filter_qc=lambda qc: message(qc, secondQubit="1"))


testSuperdenseQuantum().run()
