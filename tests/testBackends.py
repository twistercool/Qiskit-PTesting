from Qiskit_PTesting.Qiskit_PTesting import QiskitPropertyTest, TestProperty, Qarg

class testAssertEqual(QiskitPropertyTest):
    def property(self):
        return TestProperty(p_value=0.001,
                            nbTests=1,
                            nbTrials=10,
                            nbMeasurements=10,
                            nbQubits=3,
                            backend="ibmq")

    def quantumFunction(self, qc):
        qc.x(0)
        qc.h(1)
        qc.h(2)


    def assertions(self):
        self.assertEqual(1, 2)
        self.assertEqual(1, 2)



class testAssertProbability(QiskitPropertyTest):
    def property(self):
        return TestProperty(0.02,
                            6,
                            70,
                            100,
                            8,
                            1,
                            {0: Qarg(0, 0, 0, 0),
                                2: Qarg(90, 90, 150, 150),
                                3: Qarg(300, 360, 320, 350),
                                4: Qarg(90, 90, 90, 90),
                                5: Qarg(90, 90, 150, 150)
                            })

    def quantumFunction(self, qc):
        pass

    def assertions(self):
        self.assertProbability(2, 0.5)
        self.assertProbability(0, 1)
        self.assertNotProbability(0, 0.5)
        self.assertProbability(4, 0.5, basis="x")



#qiskit example code for quantum teleportation
def entanglement_bell_pair(qc, a, b):
    qc.h(a) # Put qubit a into state |+> or |-> using hadamard gate
    qc.cx(a,b) # CNOT with a as control and b as target
def alice_state_qubits(qc, psi, a):
    qc.cx(psi, a) #psi is the state of q0
    qc.h(psi)
def measure_classical_send(qc, a, b):
    qc.barrier()
    qc.measure(a,0)
    qc.measure(b,1)
def bob_apply_gates(qc, qubit, cr1, cr2):
    qc.z(qubit).c_if(cr1, 1)  #if cr1 is 1 apply Z gate
    qc.x(qubit).c_if(cr2, 1) #if cr2 is 1 apply x gate, look at table above

def quantumTeleportation(qc, qu_0, qu_1, qu_2, cl_0, cl_1):
    qc.barrier()
    entanglement_bell_pair(qc, qu_1, qu_2)
    qc.barrier()
    alice_state_qubits(qc, qu_0, qu_1)
    measure_classical_send(qc, cl_0, cl_1)
    bob_apply_gates(qc, qu_2, cl_0, 1)

class testAssertTeleported(QiskitPropertyTest):
    def property(self):
        return TestProperty(0.05,
                            15,
                            80,
                            400,
                            3,
                            2,
                            {0: Qarg(0, 360, 0, 360)},
                            "qasm_simulator")

    def quantumFunction(self, qc):
        #Quantum teleportation code from qiskit
        quantumTeleportation(qc, 0, 1, 2, 0, 1)

    def assertions(self):
        self.assertTeleported(0, 2)
        self.assertTeleported(0, 2)




class testAssertEntangled(QiskitPropertyTest):
    def property(self):
        return TestProperty(0.01,
                            10,
                            100,
                            500,
                            7,
                            0,
                            {0: Qarg(0, 0, 0, 0, True),
                                2: Qarg([1 + 0j, 0], 0, 0),
                                3: Qarg(300, 360, 320, 350),
                                4: Qarg(90, 90, 150, 150),
                                5: Qarg(90, 90, 150, 150)
                            })

    def quantumFunction(self, qc):
        qc.h(0)
        qc.cx(0, 2)

    def assertions(self):
        self.assertEntangled(0, 2)
        self.assertNotEntangled(4, 5)


testAssertEqual().runTests()
testAssertProbability().runTests()
testAssertEntangled().runTests()
testAssertTeleported().runTests()
