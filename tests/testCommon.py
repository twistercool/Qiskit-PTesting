from Qiskit_PTesting.Qiskit_PTesting import TestProperty, Qarg, QiskitPropertyTest


class testing(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=5,
                            nbClassicalBits=20)

    def quantumFunction(self, qc):
        qc.h(0)
        qc.h(1)


    def assertions(self):
        self.assertMostCommon("00000") # should fail
        self.assertMostCommon(["00000", "01000", "11000", "10000"])

testing().run()
