from Qiskit_PTesting.Qiskit_PTesting import QiskitPropertyTest, TestProperty, Qarg

class testAssertState(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=7,
                            qargs={0: Qarg(0, 0, 0, 0, True),
                                2: Qarg([1 + 0j, 0], 0, 0),
                                3: Qarg(300, 360, 320, 350),
                                4: Qarg(90, 90, 150, 150),
                                5: Qarg(90, 90, 150, 150),

                                6: Qarg("0")
                            })

    def quantumFunction(self, qc):
        qc.h(6)
        #qc.cx(0, 2)
        pass

    def assertions(self):
        self.assertState(6, 90, 0)

testAssertState().run()
