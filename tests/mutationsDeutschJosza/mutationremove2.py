from Qiskit_PTesting.Qiskit_PTesting import TestProperty, Qarg, QiskitPropertyTest
from math import sqrt
import numpy as np
from qiskit import QuantumCircuit



#input qc will need n + 1 qubits and n classical bits
def dj_oracle(case, n):
    # We need to make a QuantumCircuit object to return
    # This circuit has n+1 qubits: the size of the input,
    # plus one output qubit
    oracle_qc = QuantumCircuit(n+1)

    # First, let's deal with the case in which oracle is balanced
    if case == "balanced":
        # First generate a random number that tells us which CNOTs to
        # wrap in X-gates:
        b = np.random.randint(1,2**n)
        # Next, format 'b' as a binary string of length 'n', padded with zeros:
        b_str = format(b, '0'+str(n)+'b')
        # Next, we place the first X-gates. Each digit in our binary string
        # corresponds to a qubit, if the digit is 0, we do nothing, if it's 1
        # we apply an X-gate to that qubit:
        for qubit in range(len(b_str)):
            if b_str[qubit] == '1':
                oracle_qc.x(qubit)
        # Do the controlled-NOT gates for each qubit, using the output qubit
        # as the target:
        for qubit in range(n):
            pass
            #oracle_qc.cx(qubit, n) ###REMOVED GATE
        # Next, place the final X-gates
        for qubit in range(len(b_str)):
            if b_str[qubit] == '1':
                oracle_qc.x(qubit)
        oracle_gate = oracle_qc.to_gate()
        oracle_gate.name = "Balanced oracle" # To show when we display the circuit

    # Case in which oracle is constant
    if case == "constant":
        # First decide what the fixed output of the oracle will be
        # (either always 0 or always 1)
        output = np.random.randint(2)
        if output == 1:
            oracle_qc.x(n)
        oracle_gate = oracle_qc.to_gate()
        oracle_gate.name = "Constant oracle" # To show when we display the circuit

    return oracle_gate


def dj_algorithm(qc, string, n):
    qc.x(n)
    qc.h(n)
    # And set up the input register:
    for qubit in range(n):
        qc.h(qubit)
    # Let's append the oracle gate to our circuit:
    qc.barrier()
    qc.append(dj_oracle(string, n), range(n + 1))
    qc.barrier()
    for qubit in range(n):
        qc.h(qubit)

    return qc


def findOracle(qc):
    for gate in qc.data:
        if gate[0].name == "Constant oracle":
            return "constant"
        elif gate[0].name == "Balanced oracle":
            return "balanced"

min, max = (5, 15)
class testOracle(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=[min, max])

    def quantumFunction(self, qc):
        strs = ["balanced", "constant"]
        qc = dj_algorithm(qc, strs[np.random.randint(2)], len(qc.qubits) - 1)
        #print(qc)

    def assertions(self):
        for x in range(1, max - 1):
            self.assertEqual(0, x, filter_qc=lambda qc: len(qc.qubits) >= x + 2)

        self.assertProbability(0, 0, filter_qc=lambda qc: findOracle(qc) == "balanced")
        self.assertProbability(0, 1, filter_qc=lambda qc: findOracle(qc) == "constant")


testOracle().run()
