from Qiskit_PTesting.Qiskit_PTesting import TestProperty, Qarg, QiskitPropertyTest

from qiskit import QuantumCircuit
from qiskit.circuit.library import PhaseOracle, GroverOperator


qc = QuantumCircuit(3)
qc.h([0, 1, 2])
oracle = PhaseOracle.from_dimacs_file("3sat.dimacs")
qc = qc.compose(GroverOperator(oracle))
oracle = qc.to_gate()
oracle.name = "Oracle"

class testSatSolver(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=3)

    def quantumFunction(self, qc):
        qc.append(oracle, range(3))
        #print(qc)

    def assertions(self):
        self.assertMostCommon(["000", "101", "110"])

testSatSolver().run()

