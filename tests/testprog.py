from Qiskit_PTesting.Qiskit_PTesting import QiskitPropertyTest, TestProperty, Qarg
from math import pi

class testAssertEqual(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=6,
                            qargs={
                                0: Qarg("0"),
                                1: Qarg([1, 0], 0, 0),
                                2: Qarg("+"),
                                3: Qarg(pi/2, pi/2, 0, 0, True),
                                4: Qarg("-"),
                                5: Qarg(pi/2, pi/2, pi, pi, True)
                            })

    def quantumFunction(self, qc):
        qc.h([0, 4, 5])
        qc.x([4, 5])

    def assertions(self):
        self.assertEqual(1, 0, qu0_pre=True, qu1_pre=True)
        self.assertEqual(1, 1)
        self.assertEqual(2, 3)
        self.assertNotEqual(2, 4, basis="x")
        self.assertEqual(4, 5)
        self.assertEqual(4, 5, qu0_pre=True, qu1_pre=True)
        self.assertNotEqual(4, 5, qu0_pre=True, qu1_pre=False)
        self.assertNotEqual(4, 5, qu0_pre=False, qu1_pre=True)


class testAssertEntangled(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=7,
                            qargs={0: Qarg("0"),
                                1: Qarg([1, 0]),
                                2: Qarg("+"),
                                3: Qarg("+")
                            })

    def quantumFunction(self, qc):
        qc.h(0)
        qc.cx(0, 1)

    def assertions(self):
        self.assertEntangled(0, 1)
        self.assertNotEntangled(2, 1)
        self.assertNotEntangled(2, 3)



class testAssertProbability(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=2,
                            qargs={0: Qarg("0"),
                                1: Qarg("-")
                            })

    def assertions(self):
        self.assertProbability(0, 1)
        self.assertProbability(0, 0.5, basis="x")
        self.assertProbability(0, 0.5, basis="y")
        self.assertProbability(1, 0.5)
        self.assertProbability(1, 0, basis="x")
        self.assertProbability(1, 0.5, basis="y")
        self.assertNotProbability(0, 0.5)



#qiskit example code for quantum teleportation
def entanglement_bell_pair(qc, a, b):
    qc.h(a)
    qc.cx(a, b)
def alice_state_qubits(qc, psi, a):
    qc.cx(psi, a)
    qc.h(psi)
def measure_classical_send(qc, a, b):
    qc.barrier()
    qc.measure(a, 0)
    qc.measure(b, 1)
def bob_apply_gates(qc, qubit, cr1, cr2):
    qc.z(qubit).c_if(cr1, 1)
    qc.x(qubit).c_if(cr2, 1)

def quantumTeleportation(qc, qu_0, qu_1, qu_2, cl_0, cl_1):
    entanglement_bell_pair(qc, qu_1, qu_2)
    alice_state_qubits(qc, qu_0, qu_1)
    measure_classical_send(qc, cl_0, cl_1)
    bob_apply_gates(qc, qu_2, cl_0, 1)

class testAssertTeleported(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=3,
                            qargs={0: Qarg("any")})

    def quantumFunction(self, qc):
        quantumTeleportation(qc, 0, 1, 2, 0, 1)

    def assertions(self):
        self.assertTeleported(0, 2)





class testAssertState(QiskitPropertyTest):
    def property(self):
        return TestProperty(p_value=0.001,
                            nbQubits=1,
                            qargs={0: Qarg(90, 90, 10, 10)
                            })

    def assertions(self):
        self.assertState(0, 90, 10)
        self.assertNotState(0, 93, 8)



class testAssertMostCommon(QiskitPropertyTest):
    def property(self):
        return TestProperty(nbQubits=3)

    def quantumFunction(self, qc):
        qc.h([0, 1])

    def assertions(self):
        self.assertMostCommon(["000", "010", "110", "100"])



testAssertEqual().run()
testAssertEntangled().run()
testAssertProbability().run()
testAssertTeleported().run()
testAssertState().run()
testAssertMostCommon().run()
